import test from 'ava';
import React from 'react';
import ReactDOM from 'react-dom';

import { createStore as modCreateStore } from '../mod/selex';
import { createStore as distCreateStore } from '../dist/selex';
import { createStore as srcCreateStore } from '../src';
import { Provider as ModProvider } from '../mod/connect';
import { Provider as DistProvider } from '../dist/connect';
import { Provider as SrcProvider } from '../src/connect';
import { connect as modConnect } from '../mod/connect';
import { connect as distConnect } from '../dist/connect';
import { connect as srcConnect } from '../src/connect';

const run = (name, createStore, Provider, connect) => {
    test(`${name} connect returns consumer component`, t => {
        const Component = ({ test }) => (<div>{test}</div>);
        const map = ({ test }) => ({
            test,
        });
        const store = createStore(map, { test: 'Test' });
        const Connected = connect(map)(Component);

        document.body.innerHTML = "<div id=\"app\"></div>";
        const appDiv = document.body.querySelector('#app');

        return new Promise(resolve => {
            ReactDOM.render(
                <Provider store={store}>
                    <Connected />
                </Provider>,
                appDiv
            );
            
            store.subscribe(state => {
                t.deepEqual(state, { test: 'Test' });
                if (appDiv.innerHTML.toString() !== '') {
                    t.regex(appDiv.innerHTML.toString(), /\<div\>Test\<\/div\>/);
                    ReactDOM.unmountComponentAtNode(appDiv);
                    resolve(true);
                }
            });

            store.dispatch({ type: '@@init' });
        });
    });

    test(`${name} consumer component renders with new props`, t => {
        const Component = ({ test }) => (<div>{test}</div>);
        const map = ({
            test: ({ test }) => test,
        });
        const store = createStore((s, a) => ({ test: a.payload }), { test: 'Test' });
        const Connected = connect(map)(Component);

        document.body.innerHTML = "<div id=\"app\"></div>";
        const appDiv = document.body.querySelector('#app');

        return new Promise(resolve => {
            ReactDOM.render(
                <Provider store={store}>
                    <Connected />
                </Provider>,
                appDiv
            );

            let count = 0;
            store.subscribe(state => {
                if (appDiv.innerHTML.toString() !== '' && !count) {
                    t.deepEqual(state, { test: 'Test' });
                    t.regex(appDiv.innerHTML.toString(), /\<div\>Test\<\/div\>/);
                    count++;
                }
                if (count && state.test !== 'Test') {
                    t.deepEqual(state, { test: 'Other' });
                    t.regex(appDiv.innerHTML.toString(), /\<div\>Other\<\/div\>/);
                    ReactDOM.unmountComponentAtNode(appDiv);
                    resolve(true);
                }
            });

            store.dispatch({ type: '@@init', payload: 'Test' });
            setTimeout(() => store.dispatch({ type: '@@init', payload: 'Other' }), 500);
        });
    });
}

run('MODULE', modCreateStore, ModProvider, modConnect);
run('MAIN', distCreateStore, DistProvider, distConnect);
run('SOURCE', srcCreateStore, SrcProvider, srcConnect);
