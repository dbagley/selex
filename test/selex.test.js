import test from 'ava';
import { createEpicMiddleware } from 'redux-observable';
import 'rxjs/add/operator/map';
import { createStore as modCreateStore } from '../mod/selex';
import { createStore as distCreateStore } from '../dist/selex';
import { createStore as srcCreateStore } from '../src';

const run = (name, createStore) => {
    test(`${name} createStore`, t => {
        const store = createStore((s, a) => s, {});

        t.deepEqual(store.getState(), {});
    });

    test(`${name} createAction dispatcher`, t => {
        const store = createStore((s, a) => {
            t.deepEqual(a, { type: 'anything', payload: 'payload' });
            return s;
        }, {});

        const action = store.createAction('anything');
        action('payload');
    });

    test(`${name} createAction payload`, t => {
        const store = createStore((s, a) => {
            t.deepEqual(a, { type: 'anything', payload: { key: 1, name: 'a' } });
            return s;
        }, {});

        const action = store.createAction('anything', (key, name) => ({ key, name }));
        action(1, 'a');
    });

    test(`${name} createStore dispatching`, t => {
        const reducer = (store, action) => ({
            ...store,
            dispatched: [
                ...store.dispatched,
                action.payload
            ]
        });

        const store = createStore(reducer, {dispatched: []});
        const action = store.createAction('dispatch');

        action('one');
        action('two');
        action('three');

        t.deepEqual(
            store.getState(),
            { dispatched: ['one', 'two', 'three'] }
        );
    });

    test(`${name} old school dispatch`, t => {
        const reducer = (store, action) => ({
            ...store,
            dispatched: [
                ...store.dispatched,
                action.payload
            ]
        });

        const store = createStore(reducer, {dispatched: []});

        store.dispatch({ type: 'something', payload: 'one' });
        store.dispatch({ type: 'something', payload: 'two' });
        store.dispatch({ type: 'something', payload: 'three' });

        t.deepEqual(store.getState(), { dispatched: ['one', 'two', 'three'] });
    });

    test(`${name} Works with existing redux middleware`, t => {
        const reducer = (store, action) => ({
            ...store,
            dispatched: [
                ...store.dispatched,
                action.payload
            ],
        });

        const epicMiddleware = createEpicMiddleware();

        const store = createStore(reducer, {dispatched: []}, [epicMiddleware]);
        const action = store.createAction('dispatch');
        const epic = (action$) => action$.ofType('dispatch')
            .map(action => ({ type: 'mispatch', payload: `other-${action.payload}` }));

        epicMiddleware.run(epic);

        action('one');
        store.dispatch({ type: 'dispatch', payload: 'two' });
        action('three');

        t.deepEqual(store.getState(), {
            dispatched: [
                'one',
                'other-one',
                'two',
                'other-two',
                'three',
                'other-three',
            ]
        });
    });

    test(`${name} Reducer automatically combines.`, t => {
        const reducer = {
            dispatched: (state, action) => [
                ...(state || []),
                action.payload,
            ],
        };

        const store = createStore(reducer, {dispatched: []});
        const action = store.createAction('dispatch');

        action('one');
        action('two');
        action('three');

        t.deepEqual(
            store.getState(),
            { dispatched: ['one', 'two', 'three'] }
        );
    });

    test(`${name} Combine arrays of reducers`, t => {
        const reducer = {
            dispatched: [
                (state, action) => [
                    ...(state || []),
                    action.payload,
                ],
                (state) => [
                    ...state.reverse(),
                ],
            ],
        };

        const store = createStore(reducer, {dispatched: []});
        const action = store.createAction('dispatch');

        action('one');
        action('two');
        action('three');

        t.deepEqual(
            store.getState(),
            { dispatched: ['two', 'one', 'three'] }
        );
    });

    test(`${name} subscribe to changes to selector values`, t => {
        const reducer = {
            dispatched: [
                (state) => [
                    ...state.reverse(),
                ],
                (state, action) => [
                    ...(state || []),
                    action.payload,
                ],
            ],
        };

        const store = createStore(reducer, {dispatched: []});
        const action = store.createAction('dispatch');

        let valid;
        const validate = (value) => {
            const validated = value === valid;
            if (validated) {
                switch (valid) {
                    case undefined:
                        valid = 'one';
                        break;
                    case 'one':
                        valid = 'two';
                        break;
                    case 'two':
                        valid = 'three';
                        break;
                    case 'three':
                        valid = 'four';
                        break;
                }
            }
            return validated;
        }

        const selected = store.select(state => state.dispatched[state.dispatched.length - 1])
            .subscribe(sval => t.true(validate(sval)));

        action('one');
        action('two');
        action('three');
        action('four');
        action('five');

        t.deepEqual(
            store.getState(),
            { dispatched: ['five', 'three', 'one', 'two', 'four'] }
        );
    });
};

run('SOURCE', srcCreateStore);
run('MODULE', modCreateStore);
run('MAIN', distCreateStore);
