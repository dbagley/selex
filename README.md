# Selex

[![pipeline status](https://gitlab.com/selex/selex/badges/master/pipeline.svg)](https://gitlab.com/selex/selex/commits/master)
[![coverage report](https://gitlab.com/selex/selex/badges/master/coverage.svg)](https://gitlab.com/selex/selex/commits/master)

Selex is a predictable, observable state container for javascript applications.

It helps you write applications that behave consistently, run in different environments, and are easy to test. On top of that, it provides a great reactive programming experience.
