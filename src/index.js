import { Subject } from 'rxjs/Subject';

import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/withLatestFrom';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/scan';

/**
 * Combine a collection of reducers to a single function
 * 
 * @example
 * [EXAMPLE ../examples/combineReducers.js]
 * 
 * @function
 * @export
 * @param {Function|Object|Array} reducers The collection to combine
 * @returns {Function} The result of the collection combined
 */
export const combineReducers = (reducers) => {
    const reducerType = typeof reducers;

    const reduceArray = value => value.map(reducer => combineReducers(reducer))
        .reduce((a, b) => (state, action) => a(b(state, action), action), state => state);
    
    const reduceObject = value => Object.keys(value)
        .map(key => (state, action) => ({ [key]: combineReducers(reducers[key])(state[key], action) }))
        .reduce((a, b) => (state, action) => ({ ...a(state, action), ...b(state, action) }), () => ({}));

    const combine = value => (Array.isArray(value) ? reduceArray(value) : reduceObject(value));
    
    switch (reducerType) {
        case 'function':
            return reducers;
        case 'object':
            return combine(reducers);
        default:
            return () => reducers;
    }
}

/**
 * Apply Redux middleware to Selex createStore
 * 
 * @example
 * [EXAMPLE ../examples/applyMiddleware.js]
 * 
 * @export
 * @function
 * @param {*Function} middleware
 * @returns {Function} The new createStore
 */
export const applyMiddleware = (...middleware) => (createStore) => (...args) => {
    let dispatch = () => null;
    
    const store = createStore(...args);
    const middlewareAPI = {
        getState: store.getState,
        dispatch: (...args) => dispatch(...args),
    };

    dispatch = middleware.map(ware => ware(middlewareAPI))
        .reduce((a, b) => (func) => a(b(func)), (arg => arg))(store.dispatch);

    store.dispatch = dispatch;

    return store;
}

/**
 * Create a new store object
 * 
 * @example
 * [EXAMPLE ../examples/createStore.js]
 * 
 * @export
 * @function
 * @param {any} rootReducer 
 * @param {any} initialState 
 * @param {any} middleware 
 * @returns {Object} Store 
  */
export function createStore(rootReducer, initialState, middleware) {
    if (typeof middleware === 'function') {
        return middleware(createStore)(rootReducer, initialState);
    }

    if (Array.isArray(middleware)) {
        return applyMiddleware(...middleware)(createStore)(rootReducer, initialState);
    }

    const $action = new Subject();
    const $root = new Subject();
    const $reducer = $root.startWith(rootReducer || ((state, action) => state));

    const $state = $action
        .withLatestFrom($reducer, (action, reducer) => state => combineReducers(reducer)(state, action))
        .startWith(initialState || {})
        .scan((state, apply) => apply(state));

    let currentState = initialState;
    let stateScript = $state.subscribe((state) => {
        currentState = state;
    });

    let actions = {};

    let dispatch = (action) => $action.next(action) || action;
    const dispatcher = (func) => (...args) => dispatch(func(...args));

    const createAction = (type, payload = (arg) => arg, meta) => {
        const execute = dispatcher((...args) => ({
            type,
            payload: payload(...args),
            ...(meta ? ({ meta: meta(...args) }) : ({})),
        }));
        actions = { ...actions, [type]: execute };

        return execute;
    };

    const select = (selector, keySelect, comparator) => {
        const $subject = new Subject();
        const $selected = $subject.startWith(selector(currentState))
            .distinctUntilChanged(keySelect, comparator);
        $state.subscribe(state => $subject.next(selector(state)));
        return $selected;
    }

    const store = {
        state$: $state,
        reducer$: $reducer,
        action$: $action,
        getState: () => currentState,
        select,
        createAction,
        subscribe: (callback) => $state.subscribe(callback),
        replaceRootReducer: (reducer) => $root.next(reducer),
    };

    Object.defineProperty(store, 'dispatch', {
        get: () => dispatch,
        set: (next) => {
            dispatch = next;
        },
    })
    Object.defineProperty(store, 'actions', { get: () => ({ ...actions }) });
    Object.defineProperty(store, 'state', { get: store.getState });

    return store;
};
