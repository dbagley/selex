import React from 'react';
import ConnectComponent from './ConnectComponent';

const ConnectContext = React.createContext();

/**
 * Render children with the store in context
 * 
 * @function
 * @param {Object} props
 * @returns {Function}
 * @reactProps {Object} store The store instance for context
 */
export const Provider = ({ store, children }) => (
    <ConnectContext.Provider value={store}>
        {children}
    </ConnectContext.Provider>
);

/**
 * Create a function to generate a component with provided selectors
 * 
 * @function
 * @param {Object|Function} mapStateToProps 
 * @param {Object|Function} mapDispatchToProps
 * @returns {Function}
 */
export const connect = (mapStateToProps, mapDispatchToProps) => {
    /**
     * Returns generated component with store context and selectors in PureComponent wrapper
     * 
     * @function
     * @param {Object|Function|String} Component
     * @returns {Function}
     */
    return Component => props => (
        <ConnectContext.Consumer>
            {context => (
                <ConnectComponent store={context} mapState={mapStateToProps} mapDispatch={mapDispatchToProps}>
                    <Component {...props} />
                </ConnectComponent>
            )}
        </ConnectContext.Consumer>
    );
}
