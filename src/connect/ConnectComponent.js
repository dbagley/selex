import React from 'react';

/**
 * Subscribes to $select observables, and renders children
 * 
 * @export
 * @class ConnectComponent
 * @extends {React.PureComponent}
 * @reactProps {Object} store Instance of the store
 * @reactProps {Object|Function} mapProps Selector or collection of
 * selectors to subscribe to
 * @reactProps {Object|Function} mapDispatch Selector or collection
 * of selectors to pass dispatch to
 */
export default class ConnectComponent extends React.PureComponent {

    /**
     * Creates an instance of ConnectComponent.
     * 
     * @method ConnectComponent.constructor
     * @param {Object} props
     * @memberOf ConnectComponent
     */
    constructor(props) {
        super(props);
        this.prepareMethods = ({
            function: map => this.props.store.select(map)
                .subscribe(selected => this.setState(selected)),
            object: map => (map !== null && map !== NaN) &&
                Object.keys(map)
                    .map(key => this.props.store.select(map[key])
                        .subscribe(selected => this.setState({
                            [key]: selected
                        }))
                    )
                    .reduce((sub, next) => ({
                        unsubscribe: () => {
                            next.unsubscribe();
                            sub.unsubscribe();
                        },
                    }), { unsubscribe: () => null }),
        });

        this.prepareMethods.function = this.prepareMethods.function.bind(this);
        this.prepareMethods.object = this.prepareMethods.object.bind(this);

        this.prepareDispatch = ({
            function: map => map(this.props.store.dispatch),
            object: map => Object.keys(map)
                .map(key => ({ [key]: map[key](this.props.store.dispatch) }))
                .reduce((item, next) => ({ ...item, ...next })),
            undefined: () => ({}),
        });

        this.state = this.prepareDispatch[typeof this.props.mapDispatch](
            this.props.mapDispatch
        );
    }

    componentDidMount() {
        this.subscription = this.prepareMethods[typeof this.props.mapState](
            this.props.mapState
        );
    }

    componentWillUnmount() {
        this.subscription.unsubscribe();
        delete this.subscription;
    }

    /**
     * Render the children with props from selectors
     * 
     * @method ConnectComponent.render
     * @returns {Component|Array[Component]}
     * @memberOf ConnectComponent
     */
    render() {
        return React.Children.map(
            this.props.children,
            child => React.cloneElement(child, {
                ...child.props,
                ...this.state
            })
        );
    }

}
