import { combineReducers } from 'selex';

// TODO: Document combining arrays of reducers
export const rootReducer = combineReducers({
    stack: (state, action) => [],
    constant: 'VALUE',
});

const nextState = rootReducer({}, {});
// nextState = { stack: [], constant: 'VALUE' };
