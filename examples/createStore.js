import React from 'react';
import ReactDOM from 'react-dom';
import { createEnhancedStore as createStore, rootReducer } from './applyMiddleware';
import { combineReducers } from 'selex';

const loadedReducer = combineReducers([
    rootReducer,
    (state = {}, action) => {
        if (action.type === 'SET_LOADING_COMPLETE') {
            return {
                ...state,
                loaded: true,
            }
        }

        if (typeof state.loaded === 'undefined') {
            return {
                ...state,
                loaded: false,
            }
        }

        return state;
    },
]);

const Store = createStore(loadedReducer, { loading: false, loaded: false });
const AppComponent = ({ loading, loaded }) => {
    switch(true) {
        case loading:
            return (<div className="spinner" />);
        case loaded:
            return (<div className="content">Loaded Content</div>);
        default:
            return (<div />);
    }
};

const renderApp = state => ReactDOM.render(
    <AppComponent {...state} />,
    document.querySelector('#app')
);

Store.subscribe(renderApp); // Render whenever the store changes
Store.dispatch({ type: 'SET_LOADING' }); // Dispatch loading work
