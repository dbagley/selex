const browserEnv = require('browser-env');
browserEnv();

// TODO: Create React components for runkit output
const React = require('react');
const { createStore } = require('selex');
const cuid = require('cuid');
const { filter, mergeMap } = require('rxjs/operators');

const ADD = "ADD_TODO";
const REMOVE = "REMOVE_TODO";

const addTodoReducer = (state = [], act) => (
    act.type === ADD ? [...state, act.payload] : state
);
const addTodoGenerator = text => ({ id: cuid(), text });
const removeTodoReducer = (state = [], act) => (
    act.type === REMOVE && state.length ?
    state.filter(todo => todo.id !== act.payload) :
    state
);

// combineReducers can combine arrays of reducers.
const rootReducer = [addTodoReducer, removeTodoReducer];

// createStore automatically combines reducer.
const todoStore = createStore(rootReducer, []);

// Built in action generators bound to this stores dispatch.
const addTodo = todoStore.createAction(ADD, addTodoGenerator);
const removeTodo = todoStore.createAction(REMOVE);

// Simulate saving async
const saveTodo = async ({ id, text }) => {
    return await new Promise(resolve => {
        setTimeout(
            () => resolve(id),
            1000
        );
    });
}
// Simulate deleting async
const deleteTodo = async (id) => await new Promise(
    resolve => {
        setTimeout(
            () => resolve(id),
            1000
        );
    }
);

// Save every todo added to the store.
todoStore.action$.pipe(
    filter(action => action.type === ADD),
    mergeMap(action => saveTodo(action.payload))
).subscribe(id => {
    console.log(`SIDE EFFECT - Saved todo: ${id}`);
    removeTodo(id);
});

// Delete every todo removed from the store.
todoStore.action$.pipe(
    filter(action => action.type === REMOVE),
    mergeMap(action => deleteTodo(action.payload))
).subscribe(id => console.info(`SIDE EFFECT - Deleted todo: ${id}`));

todoStore.subscribe(val => console.info('New store value:', val));

addTodo('Make a todo app example.');
addTodo('Subscribe to anything.');
addTodo('Profit...');
