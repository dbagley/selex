module.exports.onHandleDocs = event => {
    event.data.docs.forEach(doc => {
        if (!doc.importPath) return;
        var parts = doc.importPath.split("/");
        switch (doc.kind) {
            case "variable":
            case "function":
            case "class":
                doc.importPath = parts[0] + parts[2] === 'connect' ? '/connect' : '';
                break;
        }
    });
};
